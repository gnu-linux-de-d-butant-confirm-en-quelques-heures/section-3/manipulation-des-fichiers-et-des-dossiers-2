# GNU/Linux de débutant à confirmé en quelques heures

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser les commandes permettant de manipuler les fichiers et les répertoires sur Linux.

Ainsi nous verrons les commandes suivantes:
- `cp`: commande permettant de copier des fichiers
- `mv`: commande permettant de déplacer un fichier
- `diff`: commande permettant de vérifier les différences existantes entre plusieurs fichiers

<br/>

# Introduction

Tout d'abord, je vous invite à vérifier que Docker est bien installé sur votre machine.
Pour cela tapez la commande :
`sudo apt update && sudo apt install docker docker.io -y`

Vous pouvez aussi utiliser le docker playground si vous ne souhaitez pas installer ces éléments sur votre propre machine :
https://labs.play-with-docker.com/

Pour lancer l'environnement de l'exercice, vous allez exécuter la commande :

`docker run -it --rm --name man jassouline/file2:latest bash`

Pour sortir de l'environnement, il suffit de taper la commande `exit` ou de taper sur la combinaison de touches : `CTRL + C`

<br/>

# Exercice 1

Dans ce premier exercice, nous allons essayer de comprendre comment utiliser la commande `cp` de manière simple.

1. Utilisez la commande `cp` pour copier le fichier `LPIC.pdf` situé dans le répertoire `/tmp/exercices/livres/` vers le répertoire `/tmp/exercices/animaux`:

2. Vérifiez que cela a bien fonctionné en exécutant la commande `ls /tmp/exercices/animaux`

<details><summary> Montrer la solution </summary>
<p>
cp /tmp/exercices/livres/LPIC.pdf /tmp/exercices/animaux
</p>
<p>
ls /tmp/exercices/animaux
</p>
</details>

Nous allons maintenant essayer de copier l'ensemble du répertoire `/tmp/exercices/livres` à l'intérieur du répertoire `/tmp/exercices/GNU`.

3. Essayez la commande suivante pour effectuer la tâche demandée :
`cp /tmp/exercices/livres/ /tmp/exercices/GNU/`

*(Vérifiez avec la commande `ls /tmp/exercices/GNU`)*

**Q1: Cela a-t-il fonctionné ?**
- [ ] Oui
- [ ] Non

<details><summary> Montrer la solution </summary>
<p>
Non, cela n'a pas fonctionné.
</p>
</details>

4. Essayez maintenant d'utiliser l'option `-R` de la commande `cp` pour effectuer la tâche demandée :
`cp -R /tmp/exercices/livres/ /tmp/exercices/GNU/`

*(Vérifiez avec la commande `ls /tmp/exercices/GNU`)*

**Q2: Cela a-t-il fonctionné ?**
- [ ] Oui
- [ ] Non

<details><summary> Montrer la solution </summary>
<p>
Oui, cette fois, cela a bien fonctionné !
</p>
</details>


5. Pour vérifier que vous avez correctement réussi cet exercice, tapez la commande suivante dans votre terminal :
`/tmp/copy_verify.sh`

Si vous obtenez le message : `OK !` c'est que les actions ont été correctement effectuées.

<br/>

# Exercice 2

Dans ce deuxième exercice, nous allons essayer de comprendre comment utiliser la commande `mv` de manière simple.

La commande `mv` nous permet de déplacer des fichiers.

1. Utilisez la commande `mv` pour déplacer le fichier `LPIC.pdf` situé dans le répertoire `/tmp/exercices/livres/` vers le répertoire `/tmp/exercices/GNU/linux`:

*(Vérifiez que cela a bien fonctionné en exécutant la commande `ls /tmp/exercices/GNU/linux`)*

<details><summary> Montrer la solution </summary>
<p>
mv /tmp/exercices/livres/LPIC.pdf /tmp/exercices/GNU/linux/
</p>
<p>
ls /tmp/exercices/GNU/linux
</p>
</details>

Nous allons maintenant voir qu'il est tout à fait possible d'utiliser la commande mv pour renommer un fichier. Dans notre exemple, nous allons renommer le fichier **LPIC.pdf** en **gentoo.file**

2. On se rend d'abord dans le répertoire où se trouve le fichier **LPIC.pdf** grâce à la commande: 
`cd /tmp/exercices/GNU/linux`

3. On vérifie les fichiers qui existent déjà dans ce répertoire grâce à la commande `ls`

4. On renomme le fichier grâce à la commande : 
`mv LPIC.pdf gentoo.file`

*(Vérifiez avec la commande `ls /tmp/exercices/GNU/linux`)*

**Q1: Cela a-t-il fonctionné ?**
- [ ] Oui
- [ ] Non

<details><summary> Montrer la solution </summary>
<p>
Oui, cela a bien fonctionné.
</p>
</details>

5. Pour vérifier que vous avez correctement réussi cet exercice, tapez la commande suivante dans votre terminal :
`/tmp/move_verify.sh`

Si vous obtenez le message : `OK !` c'est que les actions ont été correctement effectuées.

<br/>

# Exercice 3

Dans ce troisième exercice, nous allons essayer de comprendre comment utiliser la commande `diff` de manière simple.

La commande `diff` nous permet de comparer des fichiers.

1. Grâce à cette commande, comparez les fichiers `/tmp/exercices/GNU/linux/ubuntu.file` et `/etc/passwd` pour identifier les différences qui existent.

**Q1: Quel mot du fichier "/etc/passwd" a été modifié dans le fichier "/tmp/exercices/GNU/linux/ubuntu.file"**

<details><summary> Montrer la solution </summary>
<p>
diff /tmp/exercices/GNU/linux/ubuntu.file /etc/passwd
</p>
<p>
backups
</p>
</details>

**Q2: Quel est le numéro de la ligne au niveau de laquelle se situe cette différence dans les fichiers ?**

<details><summary> Montrer la solution </summary>
<p>
cat -n /tmp/exercices/GNU/linux/ubuntu.file
</p>
<p>
On peut d'ailleurs retrouver cette information dans le retour de la commande diff (14c14). Il s'agit en effet de la ligne 14.
</p>
</details>

<br/>

# Conclusion

Tapez la commande `exit` pour sortir du conteneur, ou bien la combinaison de touches `CTRL + c`

